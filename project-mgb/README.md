# MGB teste frontend &middot; [![Build Status](https://img.shields.io/travis/npm/npm/latest.svg?style=flat-square)](https://travis-ci.org/npm/npm) [![npm](https://img.shields.io/npm/v/npm.svg?style=flat-square)](https://www.npmjs.com/package/npm) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com) [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://github.com/your/your-project/blob/master/LICENSE)
> Additional information or tag line

A aplicação permite cadastro, edição e remoção dos usuários, todos os dados são persistidos no localhost. Completamente resposiva, faz uso de tecnologias voltadas para o desenvolmento frontend. 

## Instalação

Para subir o projeto local, basta executar os seguintes comandos

```shell
npm install || npm i
npm run serve
```

## Desenvolvimento

### Tecnologias utilizadas para o desenvolvimento

Para construção do projeto foi utilizado o framework javascript Vue.js (2.6.6), a aplicação dividida entre modulos de estilização, componentes e rotas.
Para auxiliar no desenvolvimento da aplicação, foram utilizadas as seguintes tecnologias


Dependencias:

Libs:

*  sweetlerts: 2.1.2 (exibi os feedbacks para o usuário de forma amigavel e ajuda a tornar a interação um pouco mais intuitiva)
*  vue-the-mask: 0.11.1 (utilizada para formatar os campos de input de acordo com o que é solicitado, como no campo de telefone que espera por um DDD e uma sequencia de 9 ou 8 digítos)
*  vue-toasted: 1.1.16 (exibição de toast para orientar o usuário no que ele deve fazer, foi utilizada na página de cadastro)
*  vuejs-datepicker: 1.5.4 (para estização do campo de inserção de data de nascimento)
*  vuelidate: 0.7.4 (utilizada para validar os campos, auxiliou na redução de código de validação do formulário)

Plugins:

*  Babel: 3.5.0 (compilador javascript)
*  eslint: 3.5.0 (lint para validação de regras do javascript)
*  eslint-plugin-vue: 5.0.0 (lint para validação de escrita de código do Vue)

### Setup para desenvolvedores

```shell
git clone https://gitlab.com/20Ana/mgb-teste.git
cd project-mgb/
npm install
npm npm run serve
```
