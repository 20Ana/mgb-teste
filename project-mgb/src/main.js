import Vue from 'vue'
import App from './App.vue'
import VueTheMask from 'vue-the-mask'
import router from './router'
import Toasted from 'vue-toasted';
import Vuelidate from 'vuelidate'

Vue.config.productionTip = false

Vue.use(VueTheMask)
Vue.use(Toasted)
Vue.use(Vuelidate)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')