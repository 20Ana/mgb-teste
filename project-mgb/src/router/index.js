import Vue from 'vue'
import VueRouter from 'vue-router'
import List from "../components/List.vue";
import Register from "../components/Register.vue";

Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {
            path: '*',
            name: 'init',
            redirect: '/',
            component: List
        },
        {
            path: '/',
            name: 'list',
            component: List
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/edit/:id',
            name: 'edit',
            component: Register
        }
    ]
});